<?php

namespace App\Models;

use App\Builders\HasStartEndQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder as QueryBuilder;

abstract class BaseModel extends Model
{
	/**
	 * Create a new Eloquent query builder for the model.
	 *
	 * @param QueryBuilder $query
	 * @return HasStartEndQueryBuilder
	 */
	public function newEloquentBuilder($query): HasStartEndQueryBuilder
	{
		return new HasStartEndQueryBuilder($query);
	}
}
