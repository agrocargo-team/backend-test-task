<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Foundation\Http\FormRequest;

class PaginationRequest extends FormRequest
{
	public static string $includeKey = 'include';

	public static string $limitKey = 'limit';

	public static string $startAtKey = 'start_at';

	public static string $endAtKey = 'end_at';

	public static int $defaultLimit = 10;

	public static int $maxLimit = 100;

	public function rules(): array
	{
		return [];
	}

	public function getIncludes(): array
	{
		return $this->input(static::$includeKey) ?: [];
	}

	public function getLimit(): int
	{
		$limit = (int)$this->input(static::$limitKey) ?: static::$defaultLimit;

		return ($limit < 0 || $limit > static::$maxLimit)
			? static::$defaultLimit
			: $limit;
	}

	public function getStartAt()
	{
		$default = fn() => now()->startOfMonth();
		if (!$this->has(static::$startAtKey)) {
			return $default();
		}
		try {
			return Carbon::parse($this->input(static::$startAtKey));
		} catch (InvalidFormatException $exception) {
			return $default();
		}
	}

	public function getEndAt()
	{
		$default = fn() => now()->startOfMonth();
		if (!$this->has(static::$startAtKey)) {
			return $default();
		}
		try {
			return Carbon::parse($this->input(static::$startAtKey));
		} catch (InvalidFormatException $exception) {
			return $default();
		}
	}
}
