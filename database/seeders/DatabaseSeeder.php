<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Street;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		City::factory()
			->has(
				Street::factory()->count(rand(50, 200))
			)
			->count(100)
			->create();
	}
}
